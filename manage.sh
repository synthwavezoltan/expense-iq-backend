#!/bin/bash
# shortcuts I use to make my own life easier :)
# not super fine-tuned but does the work
case $1 in
    start)
        docker-compose up $2
    ;;

    enter)
        docker-compose exec $2 bash
    ;;

    inspect)
        docker-compose ps
    ;;

    stop)
        docker-compose down
    ;;

    rebuild)
        docker-compose build
    ;;

    logs)
        docker-compose logs
    ;;

    import)
        python ./import/process.py
    ;;

    *)

    ;;
esac
