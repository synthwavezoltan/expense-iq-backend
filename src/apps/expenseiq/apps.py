from django.apps import AppConfig


class ExpenseiqConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.expenseiq"
