"""
Put the Models least used by myself to a separate file.
"""
from django.db import models

from . import Account, Category
from .mixins import BaseModel, NamedModel


class Budget(BaseModel):
    account = models.ForeignKey(
        Account, on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )
    category = models.ForeignKey(
        Category, on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )
    amount = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = "budget"


class Currency(BaseModel):
    currency_code = models.CharField(max_length=30, blank=True, null=True)
    currency_symbol = models.CharField(max_length=30, blank=True, null=True)
    placement = models.CharField(max_length=30, blank=True, null=True)
    is_default = models.CharField(max_length=30, blank=True, null=True)
    decimal_places = models.IntegerField(blank=True, null=True)
    decimal_separator = models.CharField(max_length=10, blank=True, null=True)
    group_separator = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        db_table = "currency"


class CurrencySymbol(BaseModel):
    currency_code = models.CharField(max_length=10, blank=True, null=True)
    currency_symbol = models.CharField(max_length=10, blank=True, null=True)
    is_default = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        db_table = "currency_symbol"


class License(BaseModel):
    eula_agreed = models.TextField(blank=True, null=True)
    install_date = models.IntegerField(blank=True, null=True)
    license_key = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "license"


class Passcode(BaseModel):
    passcode = models.TextField(blank=True, null=True)
    enabled = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "passcode"


class Skin(BaseModel, NamedModel):
    header_bg_start = models.TextField(blank=True, null=True)
    header_bg_end = models.TextField(blank=True, null=True)
    h1_left = models.TextField(blank=True, null=True)
    h1_right = models.TextField(blank=True, null=True)
    h2_left = models.TextField(blank=True, null=True)
    h2_right = models.TextField(blank=True, null=True)
    divider_background = models.TextField(blank=True, null=True)
    divider_text = models.TextField(blank=True, null=True)
    summary_background = models.TextField(blank=True, null=True)
    summary_text = models.TextField(blank=True, null=True)
    button_background = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "skin"


class SystemSettings(BaseModel):
    version = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "system_settings"
