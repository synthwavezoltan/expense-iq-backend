# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
from django.db import models

from apps.expenseiq.models import Category
from apps.expenseiq.models.mixins import (
    BaseModel,
    TimestampedModel,
    NamedModel,
    DescribedModel,
    ColoredModel,
)


class Account(TimestampedModel, DescribedModel, ColoredModel):
    start_balance = models.CharField(max_length=30, blank=True, null=True)
    create_date = models.CharField(max_length=30, blank=True, null=True)
    monthly_budget = models.CharField(max_length=30, blank=True, null=True)
    currency = models.CharField(max_length=10, blank=True, null=True)
    position = models.SmallIntegerField(blank=True, null=True)
    default_tran_status = models.CharField(max_length=30, blank=True, null=True)
    icon = models.CharField(max_length=30, blank=True, null=True)
    hidden = models.CharField(max_length=30, blank=True, null=True)
    exclude_from_total = models.CharField(
        max_length=30, blank=True, null=True
    )  # This field type is a guess.

    class Meta:
        db_table = "account"


class Project(TimestampedModel, DescribedModel, ColoredModel):
    budget = models.TextField(blank=True, null=True)
    activated = models.TextField(blank=True, null=True)
    currency = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "project"


class Tran(TimestampedModel):
    account = models.ForeignKey(
        Account, on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )
    category = models.ForeignKey(
        Category, on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )
    title = models.CharField(max_length=100)
    amount = models.CharField(max_length=40)
    tran_date = models.IntegerField(blank=True, null=True)
    remarks = models.TextField()
    status = models.CharField(max_length=40, blank=True, null=True)
    repeat_id = models.CharField(max_length=40, blank=True, null=True)
    photo_id = models.CharField(max_length=40, blank=True, null=True)
    split_id = models.CharField(max_length=40, blank=True, null=True)
    project_uuid = models.CharField(max_length=40, blank=True, null=True)
    transfer_account_id = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        db_table = "tran"
        verbose_name = "Transaction"
        verbose_name_plural = "Transactions"

    @property
    def tran_as_date(self):
        return self.timestamp_charfield_to_date(self.tran_date)


class Repeat(TimestampedModel):
    tran_id = models.IntegerField(blank=True, null=True)
    reminder = models.ForeignKey(
        "Reminder", on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )
    next_date = models.IntegerField(blank=True, null=True)
    repeat = models.IntegerField(blank=True, null=True)
    repeat_param = models.IntegerField(blank=True, null=True)
    type = models.TextField(blank=True, null=True)
    current = models.TextField(blank=True, null=True)
    max = models.TextField(blank=True, null=True)
    completed = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "repeat"


class Reminder(TimestampedModel):
    tran = models.ForeignKey(
        Tran, on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )
    title = models.CharField(max_length=30, blank=True, null=True)
    due_date = models.IntegerField()
    reminder_date = models.IntegerField(blank=True, null=True)
    reminder_days = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=30, blank=True, null=True)
    repeat_id = models.CharField(max_length=30, blank=True, null=True)
    payment_date = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = "reminder"


class UserSettings(TimestampedModel):
    default_reminder_days = models.SmallIntegerField(blank=True, null=True)
    reminder_time = models.CharField(max_length=40, blank=True, null=True)
    currency_symbol = models.CharField(max_length=40, blank=True, null=True)
    currency_code = models.CharField(max_length=40, blank=True, null=True)
    bills_reminder_currency = models.CharField(max_length=40, blank=True, null=True)
    autobackup_time = models.CharField(max_length=40, blank=True, null=True)
    autobackup_enabled = models.CharField(max_length=40, blank=True, null=True)
    account_balance_display = models.CharField(max_length=40, blank=True, null=True)
    forward_period = models.CharField(max_length=40, blank=True, null=True)
    forward_period_bills = models.CharField(max_length=40, blank=True, null=True)
    auto_delete_backup_enabled = models.CharField(max_length=40, blank=True, null=True)
    auto_delete_backup_days = models.CharField(max_length=40, blank=True, null=True)
    default_reporting_period = models.CharField(max_length=40, blank=True, null=True)
    default_reporting_chart_period = models.CharField(
        max_length=40, blank=True, null=True
    )
    sound_fx_enabled = models.CharField(max_length=40, blank=True, null=True)
    payment_date = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        db_table = "user_settings"
