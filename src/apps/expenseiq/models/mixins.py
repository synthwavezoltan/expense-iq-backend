from datetime import datetime

from django.db import models
from django.utils.timezone import make_aware


class BaseModel(models.Model):
    field_id = models.AutoField(
        primary_key=True, db_column="_id", blank=True
    )  # Field renamed because it started with '_'.
    uuid = models.CharField(max_length=40, unique=True)

    class Meta:
        abstract = True


class TimestampedModel(BaseModel):
    updated = models.CharField(max_length=20, blank=True, null=True)
    deleted = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        abstract = True

    def timestamp_charfield_to_date(
        self, timestamp: models.CharField | models.IntegerField
    ):
        float_stamp = float(str(timestamp)) / 1000

        if float_stamp == 0:
            return "-"

        return make_aware(datetime.fromtimestamp(float_stamp))

    @property
    def updated_as_date(self):
        return self.timestamp_charfield_to_date(self.updated)

    @property
    def deleted_as_date(self):
        return self.timestamp_charfield_to_date(self.deleted)


class NamedModel(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class DescribedModel(NamedModel):
    description = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True


class ColoredModel(models.Model):
    color = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        abstract = True
