from django.db import models

from apps.expenseiq.models.mixins import (
    TimestampedModel,
    NamedModel,
    DescribedModel,
    ColoredModel,
)


class Category(TimestampedModel, DescribedModel, ColoredModel):
    type = models.TextField(blank=True, null=True)
    parent_id = models.TextField(blank=True, null=True)  # This field type is a guess.
    icon = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "category"


class CategoryColor(TimestampedModel):
    category = models.ForeignKey(
        Category, on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )
    color_code = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        db_table = "category_color"


class CategoryTag(TimestampedModel, NamedModel):
    category = models.ForeignKey(
        Category, on_delete=models.RESTRICT, to_field="uuid", null=True, blank=True
    )

    class Meta:
        db_table = "category_tag"
