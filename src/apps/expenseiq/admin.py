from django.apps import apps
from django.contrib.admin import ModelAdmin, register, site as adminSite
from django.contrib.admin.sites import AlreadyRegistered

from .models import *


class BaseModelAdmin(ModelAdmin):
    readonly_fields = ("uuid",)


@register(Account)
class AccountAdmin(BaseModelAdmin):
    list_display = (
        "name",
        "description",
        "create_date",
        "currency",
        "position",
        "color",
        "uuid",
    )
    ordering = ("position",)


@register(Category)
class CategoryAdmin(BaseModelAdmin):
    list_display = (
        "field_id",
        "name",
        "description",
        "type",
        "color",
        "icon",
        "updated_as_date",
        "deleted_as_date",
        "uuid",
    )


@register(Project)
class ProjectAdmin(BaseModelAdmin):
    list_display = (
        "field_id",
        "name",
        "description",
        "budget",
        "activated",
        "currency",
        "color",
        "updated_as_date",
        "deleted_as_date",
    )


@register(Reminder)
class ReminderAdmin(BaseModelAdmin):
    list_display = (
        "field_id",
        "tran",
        "title",
        "due_date",
        "reminder_date",
        "reminder_days",
        "status",
        "repeat_id",
        "payment_date",
        "updated_as_date",
        "deleted_as_date",
    )


@register(CategoryColor)
class CategoryColorAdmin(BaseModelAdmin):
    list_display = (
        "field_id",
        "color_code",
        "category",
        "deleted",
        "updated_as_date",
        "deleted_as_date",
    )


@register(CategoryTag)
class CategoryTagAdmin(BaseModelAdmin):
    list_display = (
        "field_id",
        "name",
        "category",
        "updated_as_date",
        "deleted_as_date",
    )


@register(Tran)
class TranAdmin(BaseModelAdmin):
    list_display = (
        "field_id",
        "title",
        "amount",
        "account",
        "category",
        "tran_as_date",
        "status",
        "updated_as_date",
        "deleted_as_date",
    )


app_models = apps.get_app_config("expenseiq").get_models()
for model in app_models:
    try:
        adminSite.register(model)
    except AlreadyRegistered:
        pass
