# Generated from the SQL code found here: https://github.com/vmkernel/expense-iq
# It was way too outdated, so I manually added unhandled fields.
# This file is left here as a reference point only.

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Account(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    name = models.TextField()
    description = models.TextField()
    start_balance = models.TextField()  # This field type is a guess.
    create_date = models.IntegerField()
    monthly_budget = models.TextField(
        blank=True, null=True
    )  # This field type is a guess.
    currency = models.TextField(blank=True, null=True)  # This field type is a guess.
    position = models.TextField(blank=True, null=True)  # This field type is a guess.
    default_tran_status = models.TextField(blank=True, null=True)
    exclude_from_total = models.TextField(
        blank=True, null=True
    )  # This field type is a guess.

    class Meta:
        managed = False
        db_table = "account"


class Budget(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    account_id = models.IntegerField(blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)
    amount = models.TextField()  # This field type is a guess.

    class Meta:
        managed = False
        db_table = "budget"


class Category(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    name = models.TextField()
    description = models.TextField(blank=True, null=True)
    color = models.TextField()
    type = models.TextField(blank=True, null=True)
    parent_id = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = "category"


class CategoryColor(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    category_id = models.IntegerField(blank=True, null=True)
    color_code = models.TextField()

    class Meta:
        managed = False
        db_table = "category_color"


class CategoryTag(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    category_id = models.IntegerField()
    name = models.TextField()

    class Meta:
        managed = False
        db_table = "category_tag"


class Currency(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    currency_code = models.TextField(blank=True, null=True)
    currency_symbol = models.TextField(blank=True, null=True)
    placement = models.TextField(blank=True, null=True)
    is_default = models.TextField(blank=True, null=True)
    decimal_places = models.IntegerField(blank=True, null=True)
    decimal_separator = models.TextField(blank=True, null=True)
    group_separator = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "currency"


class CurrencySymbol(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    currency_code = models.TextField(blank=True, null=True)
    currency_symbol = models.TextField(blank=True, null=True)
    is_default = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "currency_symbol"


class License(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    eula_agreed = models.TextField(blank=True, null=True)
    install_date = models.IntegerField(blank=True, null=True)
    license_key = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "license"


class Passcode(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    passcode = models.TextField(blank=True, null=True)
    enabled = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "passcode"


class Project(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    name = models.TextField()
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "project"


class Reminder(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    tran_id = models.IntegerField()
    title = models.TextField()
    due_date = models.IntegerField()
    reminder_date = models.IntegerField(blank=True, null=True)
    reminder_days = models.IntegerField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    repeat_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "reminder"


class Repeat(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    tran_id = models.IntegerField(blank=True, null=True)
    reminder_id = models.IntegerField(blank=True, null=True)
    next_date = models.IntegerField(blank=True, null=True)
    repeat = models.IntegerField(blank=True, null=True)
    repeat_param = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "repeat"


class Skin(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    name = models.TextField(blank=True, null=True)
    header_bg_start = models.TextField(blank=True, null=True)
    header_bg_end = models.TextField(blank=True, null=True)
    h1_left = models.TextField(blank=True, null=True)
    h1_right = models.TextField(blank=True, null=True)
    h2_left = models.TextField(blank=True, null=True)
    h2_right = models.TextField(blank=True, null=True)
    divider_background = models.TextField(blank=True, null=True)
    divider_text = models.TextField(blank=True, null=True)
    summary_background = models.TextField(blank=True, null=True)
    summary_text = models.TextField(blank=True, null=True)
    button_background = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "skin"


class SystemSettings(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    version = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "system_settings"


class Tran(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    account_id = models.IntegerField(blank=True, null=True)
    title = models.TextField()
    amount = models.TextField()  # This field type is a guess.
    tran_date = models.IntegerField(blank=True, null=True)
    remarks = models.TextField()
    category_id = models.IntegerField(blank=True, null=True)
    status = models.TextField(blank=True, null=True)
    repeat_id = models.IntegerField(blank=True, null=True)
    photo_id = models.IntegerField(blank=True, null=True)
    split_id = models.IntegerField(blank=True, null=True)
    transfer_account_id = models.TextField(
        blank=True, null=True
    )  # This field type is a guess.

    class Meta:
        managed = False
        db_table = "tran"


class UserSettings(models.Model):
    field_id = models.AutoField(
        db_column="_id", blank=True, null=True
    )  # Field renamed because it started with '_'.
    default_reminder_days = models.IntegerField(blank=True, null=True)
    reminder_time = models.TextField(blank=True, null=True)
    currency_symbol = models.TextField(blank=True, null=True)
    currency_code = models.TextField(blank=True, null=True)
    bills_reminder_currency = models.TextField(blank=True, null=True)
    autobackup_time = models.TextField(blank=True, null=True)
    autobackup_enabled = models.TextField(blank=True, null=True)
    account_balance_display = models.TextField(blank=True, null=True)
    forward_period = models.TextField(blank=True, null=True)
    forward_period_bills = models.TextField(blank=True, null=True)
    auto_delete_backup_enabled = models.TextField(blank=True, null=True)
    auto_delete_backup_days = models.IntegerField(blank=True, null=True)
    default_reporting_period = models.TextField(blank=True, null=True)
    default_reporting_chart_period = models.TextField(blank=True, null=True)
    sound_fx_enabled = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "user_settings"
