import os
import sys

_BAK_FILE_ENDIAN = "little"
_CWD = os.getcwd()
_FLIP_BYTES = sys.byteorder == _BAK_FILE_ENDIAN

def decode_line(line):
    processed_line = line

    if _FLIP_BYTES:
        processed_line = line[::-1]

    hexline = bytearray.fromhex(processed_line)
    return hexline.decode("utf-8")


print("Start")
print(f"Python endian is: {sys.byteorder}")

if _FLIP_BYTES:
    print(".bak line hex data will be flipped.")

print("-"*40)


_INPUT = f"{_CWD}/import/sample.bak"
_OUTPUT = f"{_CWD}/export/result.sql"

with open(_INPUT, 'r') as sample:
    count = 1

    with open(_OUTPUT, 'w') as result:
        for raw_line in sample:
            print(f"line {count}...")

            try:
                stringline = decode_line(raw_line)

                result.write(f"{stringline};\n\n")
            except Exception as e:
                print(str(e))

            count = count + 1

